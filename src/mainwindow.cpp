/*
    Copyright (C) 2019 Volker Krause <vkrause@kde.org>

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <KTcpSocket>
#include <KIO/SslUi>
#include <KIO/TransferJob>
#include <kio_version.h>

#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSslSocket>
#include <QUrl>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // QNAM
    connect(ui->qnamRedirectBox, qOverload<int>(&QComboBox::currentIndexChanged), this, [this](int redirectPolicy) {
        m_qnam.setRedirectPolicy(static_cast<QNetworkRequest::RedirectPolicy>(redirectPolicy));
    });
    connect(ui->qnamHstsEnabled, &QCheckBox::toggled, this, [this](bool enabled) {
        m_qnam.setStrictTransportSecurityEnabled(enabled);
        m_qnam.enableStrictTransportSecurityStore(enabled);
    });
    connect(ui->qnamButton, &QPushButton::clicked, this, [this]() {
        ui->resultView->clear();
        auto reply = m_qnam.get(QNetworkRequest(QUrl(ui->hostBox->currentText())));
        connect(reply, &QNetworkReply::redirected, this, [this](const QUrl &url) {
            ui->resultView->appendPlainText(tr("Redirected to: %1").arg(url.toString()));
        });
        connect(reply, &QNetworkReply::sslErrors, this, [this, reply](const auto &errors) {
            for (const auto &error : errors) {
                ui->resultView->appendPlainText(error.errorString());
            }

#if KIO_VERSION_MINOR >= 62
            if (ui->qnamWithKIOSslUi->isChecked()) {
                KSslErrorUiData errorData(reply, errors);
                if (KIO::SslUi::askIgnoreSslErrors(errorData)) {
                    reply->ignoreSslErrors();
                }
            }
#else
#warning "KIO too old for QNAM SSL error handling!"
#endif
        });
        connect(reply, &QNetworkReply::finished, this, [this, reply]() {
            reply->deleteLater();
            if (reply->error() != QNetworkReply::NoError) {
                ui->resultView->appendPlainText(reply->errorString());
                return;
            }
            ui->resultView->appendPlainText(reply->request().url().toString());
            ui->resultView->appendPlainText(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString() + QLatin1String(" ") + reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString());
            const auto headers = reply->rawHeaderPairs();
            for (const auto &header : headers) {
                ui->resultView->appendPlainText(header.first + ": " + header.second);
            }

        });
    });

    // KIO
    connect(ui->kioButton, &QPushButton::clicked, this, [this]() {
        ui->resultView->clear();
        auto job = KIO::get(QUrl(ui->hostBox->currentText()));
        connect(job, &KIO::TransferJob::redirection, this, [this](KIO::Job*, const QUrl &url) {
            ui->resultView->appendPlainText(tr("Redirected to: %1").arg(url.toString()));
        });
        connect(job, &KJob::finished, this, [this, job]() {
            if (job->error() != KJob::NoError) {
                ui->resultView->appendPlainText(job->errorString());
            }
            const auto md = job->metaData();
            for (auto it = md.begin(); it != md.end(); ++it) {
                ui->resultView->appendPlainText(it.key() + QLatin1String(": ") + it.value());
            }
        });
    });

    // KTcpSocket
    connect(ui->ktcpButton, &QPushButton::clicked, this, [this]() {
        ui->resultView->clear();
        auto socket = new KTcpSocket(this);
        if (ui->kctpUseSecureProtocols->isChecked()) {
            socket->setAdvertisedSslVersion(KTcpSocket::SecureProtocols); // ### this isn't the default!?
        }
        connect(socket, &KTcpSocket::connected, this, [this]() {
            ui->resultView->appendPlainText(tr("Connected!"));
        });
        connect(socket, qOverload<KTcpSocket::Error>(&KTcpSocket::error), this, [this, socket]() {
            ui->resultView->appendPlainText(tr("Error: %1").arg(socket->errorString()));
            socket->close();
            socket->deleteLater();
        });
        connect(socket, qOverload<const QList<KSslError>&>(&KTcpSocket::sslErrors), this, [this, socket](const auto &errors) {
            for (const auto &error : errors) {
                ui->resultView->appendPlainText(error.errorString());
            }

            if (ui->ktcpWithKIOSslUi->isChecked()) {
                KSslErrorUiData errorData(socket);
                if (KIO::SslUi::askIgnoreSslErrors(errorData)) {
                    socket->ignoreSslErrors();
                }
            }
        });
        connect(socket, &KTcpSocket::encrypted, this, [this, socket]() {
            ui->resultView->appendPlainText(tr("Encrypted!"));
            socket->close();
            socket->deleteLater();
        });

        QUrl url(ui->hostBox->currentText());
        socket->connectToHostEncrypted(url.host(), url.port(443));
    });

    // QSsslSocket
    connect(ui->qsslButton, &QPushButton::clicked, this, [this]() {
        ui->resultView->clear();
        auto socket = new QSslSocket(this);
        connect(socket, &QSslSocket::connected, this, [this]() {
            ui->resultView->appendPlainText(tr("Connected!"));
        });
        connect(socket, qOverload<QAbstractSocket::SocketError>(&QSslSocket::error), this, [this, socket]() {
            ui->resultView->appendPlainText(tr("Error: %1").arg(socket->errorString()));
            socket->close();
            socket->deleteLater();
        });
        connect(socket, qOverload<const QList<QSslError>&>(&QSslSocket::sslErrors), this, [this, socket](const auto &errors) {
            for (const auto &error : errors) {
                ui->resultView->appendPlainText(error.errorString());
            }

            if (ui->qsslWithKIOSslUi->isChecked()) {
                KSslErrorUiData errorData(socket);
                if (KIO::SslUi::askIgnoreSslErrors(errorData)) {
                    socket->ignoreSslErrors();
                }
            }
        });
        connect(socket, &QSslSocket::encrypted, this, [this, socket]() {
            ui->resultView->appendPlainText(tr("Encrypted!"));
            socket->close();
            socket->deleteLater();
        });

        QUrl url(ui->hostBox->currentText());
        socket->connectToHostEncrypted(url.host(), url.port(443));
    });
}

MainWindow::~MainWindow() = default;
