add_executable(http-demo
    main.cpp
    mainwindow.cpp
)
target_link_libraries(http-demo Qt5::Widgets KF5::KIOWidgets)
